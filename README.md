# Datalab website

## Setup development environment

First clone the repository with:

```bash
git clone git@gitlab.com:purmerend/datalab/website.git datalab-website
```

Then start the Docker compose server with:

```bash
cd datalab-website/
docker-compose up
```

The server should now be available at http://localhost:8001.
